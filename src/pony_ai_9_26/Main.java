package pony_ai_9_26;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        method3();
    }

    /*

     */
    public static void method1() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums = new int[n];
        for(int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
        }
        long res = 0;
        Arrays.sort(nums);
        for(int i = 1; i < n; i++) {
            long dic = nums[i] - nums[i - 1];
            res += Math.pow(dic, 2);
        }
        System.out.println(res);
    }

    /*

     */
    public static void method2() {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        char[] cs = s.toCharArray();
        int l = 1;
        int r = s.length() / 2 + 1;
        while(l < r) {
            int mid = l + (r - l) / 2;
            if(checkLen(cs, mid)) {
                r = mid;
            } else {
                l = mid + 1;
            }
        }
        System.out.println(l);
    }

    public static boolean checkLen(char[] cs, int len) {
        int[] init = new int[27];
        for(int i = 0; i < len; i++) {
            init[cs[i] - 'a']++;
        }
        int[] cur = Arrays.copyOf(init, 27);
        for(int i = 1; i < cs.length - len; i++) {
            cur[cs[i - 1] - 'a']--;
            cur[cs[i + len - 1] - 'a']++;
            init = joint(init, cur);
            if(init[26] == 0) {
                return false;
            }
        }
        return init[26] > 0;
    }

    public static int[] joint(int[] m1, int[] m2) {
        int[] res = new int[27];
        boolean flag = false;
        for(int i = 0; i < 26; i++) {
            if(m1[i] >0 && m2[i] > 0) {
                res[i] = 1;
                flag = true;
            } else {
                res[i] = 0;
            }
        }
        res[26] = flag ? 1 : 0;
        return res;
    }

    /*

     */
    public static void method3() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] cost = new int[n];
        for(int i = 0; i < n; i++) {
            cost[i] = sc.nextInt();
        }
        DSU u = new DSU(n);
        for(int i = 0; i < m; i++) {
            int x = sc.nextInt() - 1;
            int y = sc.nextInt() - 1;
            u.join(x, y);
        }
        long res = 0;
        for(int i = 0; i < n; i++) {
            if(u.party[i] == null) {
                continue;
            }
            int c = Integer.MAX_VALUE;
            for(int p: u.party[i]) {
                c = Math.min(c, cost[p]);
            }
            res += c;
        }
        System.out.println(res);
    }

    public static class DSU {
        int[] parent;
        List<Integer>[] party;

        public DSU(int n) {
            parent = new int[n];
            party = new List[n];
            for(int i = 0; i < n ; i++) {
                parent[i] = i;
                party[i] = new ArrayList<>();
                party[i].add(i);
            }
        }

        public int find(int i) {
            if(parent[i] != i) {
                parent[i] = find(parent[i]);
            }
            return parent[i];
        }

        public void join(int i, int j) {
            int p1 = find(i);
            int p2 = find(j);
            if(p1 == p2) {
                return;
            }
            parent[p2] = p1;
            party[p1].addAll(party[p2]);
            party[p2] = null;
        }
    }

    /*

     */
    public static void method4() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums = new int[n];
        for(int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
        }

    }
}
