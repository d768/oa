package China_mobile_research;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        List<String> res = new ArrayList<>();
        helper(res, new StringBuilder(), new boolean[input.length()], input);
        Collections.sort(res);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for(int i = 0; i < res.size(); i++) {
            if(i != 0) {
                sb.append(", ");
            }
            sb.append(res.get(i));
        }
        sb.append(']');
        System.out.println(sb);
    }

    public static void helper(List<String> res, StringBuilder sb, boolean[] visited, String s) {
        if(sb.length() == s.length()) {
            res.add(sb.toString());
            return;
        }
        for(int i = 0; i < s.length(); i++) {
            if(!visited[i]) {
                sb.append(s.charAt(i));
                visited[i] = true;
                helper(res, sb, visited, s);
                visited[i] = false;
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }
}
