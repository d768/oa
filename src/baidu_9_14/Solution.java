package baidu_9_14;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Solution o = new Solution();
        o.method2();
    }

    /*
    input:
    n k
    output:
    1*n - k*n的k个乘积反转后最大数字
    100%
     */
    public void method1() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        PriorityQueue<Integer> q = new PriorityQueue<>((o1, o2) -> {
            return o2 - o1;
        });
        for(int i = 1; i <= k; i++) {
            int p = i * n;
            q.offer(reverse(p));
        }
        System.out.println(q.poll());
    }

    private int reverse(int num) {
        int res = 0;
        while(num > 0) {
            int digit = num % 10;
            res = res * 10 + digit;
            num /= 10;
        }
        return res;
    }

    /*
    n个人排队买商品，位置为1-n，第一个人买完商品，后面的人都会往前走，但是有人会忘记走
    input:
    n q: n个人，你前面q个人
    第二行：q个整数，每次第一个人买完商品后向前走的人数
    output：
    q个整数，表示每次第一个人买完商品后，全队移动的步数
    example：
        input：
        5 3             5 3
        3 3 2           0 0 1
        output:
        3 4 2           0 1 5
   50%
     */
    public void method2() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int q = sc.nextInt();
        int[] nums = new int[q];
        for(int i = 0; i < q; i++) {
            nums[i] = sc.nextInt();
        }
        int status = (1 << n) - 1;
        for(int i = 0; i < q; i++) {
            int num = nums[i];
            int res = 0;
            int count = 0;
            int index = 0;
            int indexNum = 1;
            while((status & indexNum) == 0) {
                index++;
                indexNum <<= 1;
            }
            res += index;
            status ^= indexNum;
            index++;
            indexNum <<= 1;
            int countNum = 1;
            while(count < num) {
                if((status & indexNum) != 0) {
                    res += (index - count);
                    status ^= countNum;
                    status ^= indexNum;
                    count++;
                    countNum <<= 1;
                }
                index++;
                indexNum <<= 1;
            }
            System.out.print(res);
            if(i != q - 1) {
                System.out.print(" ");
            }
        }
    }

    /*
    太长太难读了
     */
    public void method3() {
        Scanner sc = new Scanner(System.in);

    }
}
