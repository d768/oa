package jingdong_9_11;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution o = new Solution();
        o.method2();
    }

    /*
    输入：
    m*n的keyboard，x, y, z 分别是前进一步、转方向，按下操作的耗时
    接下来m*n是不同的char
    最后给一个字符串
    起始位置在左上角
    输出：
    打印出这个字符串的最小时间
    100%
     */
    private void method1() {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int x = sc.nextInt();
        int y = sc.nextInt();
        int z = sc.nextInt();
        char[][] board = new char[m][n];
        sc.nextLine();
        Map<Character, int[]> map = new HashMap<>();
        for(int i = 0; i < m; i++) {
            String line = sc.nextLine();
            board[i] = line.toCharArray();
            for(int j = 0; j < n; j++) {
                map.put(board[i][j], new int[]{i, j});
            }
        }
        String path = sc.nextLine();
        long res = 0;
        int[] pre = new int[]{0, 0};
        for(char c: path.toCharArray()) {
            int[] pos = map.get(c);
            res += getCost(pre, pos, x, y, z);
            pre = pos;
        }
        System.out.println(res);
    }

    private long getCost(int[] start, int[] target, int a, int b, int c) {
        long x = start[0], y = start[1];
        long i = target[0], j = target[1];
        if(x == i) {
            return Math.abs(y - j) * a + c;
        }
        if(y == j) {
            return Math.abs(x - i) * a + c;
        }
        return (Math.abs(x - i) + Math.abs(y - j)) * a + b + c;

    }

    /*
    服务之间存在互相依赖，a依赖b，a启动的时候，b也会随之启动，b关闭之后，a也会关闭
    输入：
    第一行n和q，分别代表服务数量，请求数量
    接下来n行，表示1-n个服务的依赖，第一个数字表示第i个服务依赖服务的数量m，后面m个数字表示依赖的服务
    接下来q行，每行两个数字，第一个表示对服务操作，0表示关闭，1表示开启，后面一个数字表示该操作生效的服务
    输出：
    q行，每一个请求后，启动着的服务的数量
    100%
     */
    private void method2() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int qnum = sc.nextInt();
        Map<Integer, List<Integer>> dep = new HashMap<>();
        Map<Integer, List<Integer>> revDep = new HashMap<>();
        for(int i = 1; i <= n; i++) {
            int num = sc.nextInt();
            List<Integer> list = new ArrayList<>();
            for(int j = 0; j < num; j++) {
                int next = sc.nextInt();
                list.add(next);
                List<Integer> l = revDep.getOrDefault(next, new ArrayList<>());
                l.add(i);
                revDep.put(next, l);
            }
            dep.put(i, list);
        }
        int[][] qs = new int[qnum][2];
        for(int i = 0; i < qnum; i++) {
            qs[i][0] = sc.nextInt();
            qs[i][1] = sc.nextInt();
        }
        int res = 0;
        boolean[] status = new boolean[n + 1];
        for(int i = 0; i < qnum; i++) {
            int op = qs[i][0];
            int index = qs[i][1];
            //turn off
            if(op == 0) {
                int diff = ops(status, index, revDep, false);
                res -= diff;
                System.out.println(res);
            } else {
                int diff = ops(status, index, dep, true);
                res += diff;
                System.out.println(res);
            }
        }
    }

    private int ops(boolean[] status, int index, Map<Integer, List<Integer>> map, boolean ops) {
        int res = 0;
        LinkedList<Integer> q = new LinkedList<>();
        q.offer(index);
        while(!q.isEmpty()) {
            int size = q.size();
            for(int i = 0; i < size; i++) {
                int cur = q.poll();
                if(status[cur] == ops) {
                    continue;
                }
                res++;
                status[cur] = ops;
                for(int next: map.getOrDefault(cur, new ArrayList<>())) {
                    q.offer(next);
                }
            }
        }
        return res;
    }


}
