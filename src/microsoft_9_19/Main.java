package microsoft_9_19;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Main o = new Main();
        System.out.println(o.method3(new int[]{4, 1, 5, 3}, 5, 2));
    }

    /*
    给一个三位数N，和可以操作数位加一的次数K，每一次操作可以给N的任意digit加一
    范围操作后最大的三位数
     */
    public int method1(int N, int K) {
        String s = String.valueOf(N);
        char[] cs = s.toCharArray();
        for(int i = 0; i < cs.length; i++) {
            if(K == 0) {
                break;
            }
            int count = '9' - cs[i];
            if(K > count) {
                K -= count;
                cs[i] = '9';
            } else {
                cs[i] += K;
                K = 0;
            }
        }
        String res = new String(cs);
        return Integer.valueOf(res);
    }

    /*
    给A和B长度的两个木棍，切割组成正方形
    返回能组成正方形的最大边长
     */
    public int method2(int A, int B) {
        if(A > B) {
            return method2(B, A);
        }
        int res = 0;
        //get two from each stick
        res = Math.max(Math.min(A / 2, B / 2), res);
        //get three from longer stick
        res = Math.max(Math.min(A, B / 3), res);
        //get four from longer stick
        res = Math.max(B / 4, res);
        return res;
    }

    /*
    A是n个住户的用电量，用户i可以装两种太阳能板，第一种花费X，产生A[i]相等的电量，第二种花费Y，产生2*A[i]电量，多余的可以给其他住户使用
    返回cover所有住户电量下，装太阳能板的最小花费
     */
    public int method3(int[] A, int X, int Y) {
        int n = A.length;
        int cost = n * X;
        Arrays.sort(A);
        int[] sum = new int[n];
        sum[0] = A[0];
        for(int i = 1; i < n; i++) {
            sum[i] = sum[i - 1] + A[i];
        }
        int index = 0;
        for(int i = n - 1; i >= 0; i--) {
            int temp = i * X + (n - i) * Y;
            int pre = i == 0 ? 0 : sum[i - 1];
            int extra = sum[n - 1] - pre;
            while(index < i && sum[index] <= extra) {
                index++;
            }
            temp -= index * X;
            if(temp > cost) {
                break;
            }
            cost = temp;
            if(index == i) {
                break;
            }
        }
        return cost;
    }
}
