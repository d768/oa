package netEase_9_18;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        method4();
    }

    /*
    给一个整数，输出各个数位能整除该数字的个数
     */
    private static void method1() {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int res = 0, copy = num;
        while(copy > 0) {
            int digit = copy % 10;
            if(digit != 0 && num % digit == 0) {
                res++;
            }
            copy /= 10;
        }
        System.out.println(res);
    }

    /*
    a-z环形键盘，敲击、左移右移一步，或者无范围移动都消耗1单位时间，无范围移动移动使用，要连续使用m次，
    给定单词和m，返回最小耗时敲完这个单词
     */
    private static void method2() {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String[] ss = input.split(" ");
        String s = ss[0];
        int m = ss.length == 1 ? 0 : Integer.valueOf(ss[1]);
        int[][] dp = new int[m + 1][s.length()];
        for(int[] line: dp) {
            Arrays.fill(line, (int)1e7);
        }
        dp[0][0] = 1;
        for(int i = 1; i < s.length(); i++) {
            char cur = s.charAt(i);
            char pre = s.charAt(i - 1);
            int len = Math.abs(pre - cur);
            len = Math.min(len, 26 - len);
            for(int j = 0; j <= m; j++) {
                if(j == 0) {
                    dp[j][i] = dp[j][i - 1] + len + 1;
                } else {
                    if(dp[j - 1][i - 1] != (int)1e7) {
                        if(j == m) {
                            dp[j][i] = Math.min(dp[j][i - 1] + len + 1, dp[j - 1][i - 1] + 2);
                        } else {
                            dp[j][i] = dp[j - 1][i - 1] + 2;
                        }
                    }
                }
            }
        }
        System.out.println(Math.min(dp[0][s.length() - 1], dp[m][s.length() - 1]));
    }

    /*
    给一个数字，要么用2^x1 + 2^x2 + 2^x3....，要么用2^x1 - 2^x2 - 2^x3....区组成他
    返回最少的使用的x的个数
     */
    private static void method3() {
        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        int one = 0;
        int count = 0;
        int zero = 0;
        boolean flag = true;
        while(num > 0) {
            if((num & 1) == 1) {
                flag = false;
                one++;
            } else if(flag) {
                zero++;
            }
            num >>>= 1;
            count++;
        }
        int res = one;
        if(zero == 0) {
            res = Math.min(res, count - one + 2);
        } else {
            res = Math.min(res, count - one - zero + 2);
        }
        System.out.println(res);
    }


    /*
    二维board，有. # *三种，.正常通过，#可以话a时间拆掉，*可以花b时间瞬移到其他*
    返回左上角到右下角最短用时
     */
    static int[][] dp;
    static int n, a, b;
    private static void method4() {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        a = sc.nextInt();
        b = sc.nextInt();
        sc.nextLine();
        String[] ss = new String[n];
        for(int i = 0;  i< n; i++) {
            ss[i] = sc.nextLine();
        }
        char[][] board = new char[n][n];
        List<int[]> star = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            board[i] = ss[i].toCharArray();
            for(int j = 0; j < n; j++) {
                if(board[i][j] == '*') {
                    star.add(new int[]{i, j});
                }
            }
        }
        dp = new int[n][n];
        for(int i = 0; i < n; i++) {
            Arrays.fill(dp[i], 1000000);
        }
        dp[0][0] = 0;
//        dp[n - 1][n - 1] = 0;
//        boolean[][] visited = new boolean[n][n];
//        visited[0][0] = true;
//        int res = dfs(board, 0, 0, star, visited);
        int[][] dir = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                for(int[] d: dir) {
                    int x = i + d[0];
                    int y = j + d[1];
                    if(x < 0 || x >= n || y < 0 || y >= n) {
                        continue;
                    }
                    int cost = board[x][y] == '#' ? a : 0;
                    dp[i][j] = Math.min(dp[i][j], cost + dp[x][y]);
                    if(board[i][j] == '*') {
                        for(int[] pos: star) {
                            dp[i][j] = Math.min(dp[pos[0]][pos[1]] + b, dp[i][j]);
                        }
                    }
                }
            }
        }
        System.out.println(dp[n - 1][n - 1]);
    }

    private static int dfs(char[][] board, int i, int j, List<int[]> star, boolean[][] visited) {
        if(dp[i][j] != 1000000) {
            return dp[i][j];
        }
        int[][] dir = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        for(int[] d: dir) {
            int x = i + d[0];
            int y = j + d[1];
            if(x < 0 || x >= n || y < 0 || y >= n || visited[x][y]) {
                continue;
            }
            visited[x][y] = true;
            int cost = dfs(board, x, y, star, visited);
            visited[x][y] = false;
            if(board[x][y] == '#') {
                cost += a;
            }
            dp[i][j] = Math.min(dp[i][j], cost);
            if(board[i][j] == '*') {
                for(int[] pos: star) {
                    dp[i][j] = Math.min(dp[pos[0]][pos[1]] + b, dp[i][j]);
                }
            }
        }
        return dp[i][j];
    }
}

