package meituan9_18;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        method5();
    }

    /*
    18%
    小团有一个长度为n的只包含小写字母的字符串。他定义一个字符串是优美的，当且仅当这个字符串的字符的所有排列中，存在一种排列使得这个串不是回文串。
    定义一个串是回文串，当且仅当从左往右与从右往左读出的字符串相同，例如a,aba,abba都是回文串，但ab,abcb不是。
    第一行一个正整数T，表示有T组数据。
    接下来对于每一组数据，一个仅包含小写字母的字符串。
    设该字符串长度为n，则：
    对于100%的数据，1≤n≤105,1≤T≤5。
    输出仅一行，如果这个字符串是优美的，输出YES；否则，输出NO。

    2
    cabac
    a

    YES
    NO
     */
    private static void method1() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] ss = new String[n];
        sc.nextLine();
        for(int i = 0; i < n; i++) {
            ss[i] = sc.nextLine();
        }
        String[] res = new String[n];
        String yes = "YES";
        String no = "NO";
        for(int i = 0; i < n; i++) {
            if(checkString(ss[i])) {
                res[i] = yes;
            } else {
                res[i] = no;
            }
        }
        for(int i = 0; i < n; i++) {
            System.out.println(res[i]);
        }
    }

    private static boolean checkString(String s) {
        int count = 0;
        int[] map = new int[26];
        for(char c: s.toCharArray()) {
            int index = c - 'a';
            if(map[index] == 0) {
                count++;
            }
            map[index]++;
        }
        if(count == 1) {
            return false;
        }
        for(int i = 0; i < 26; i++) {
            if((map[i] & 1) == 1) {
                return true;
            }
        }
        return false;
    }

    /*
    100%
    小美很喜欢求和符号Σ（读作sigma）。她随手画了一张n个点m条边的简单无向图（无重边和自环，但不保证连通），她想知道这张图有多少个形如Σ的图形。
    由于这张图可以随意拉伸旋转等变换，故这个图形需要满足如下条件：

    恰好五个点组成，这五个点互不相同

    这五个点连成一条链

    两个图形视为不同，当且仅当其包含的点的编号集合不同。具体例子参见样例。
    第一行两个正整数n,m；
    接下来m行，每行两个正整数x,y，表示点x与y之间有一条无向边。
    6 7
    1 2
    2 3
    3 4
    4 5
    5 6
    1 6
    3 6

    6
     */
    private static void method2() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        Map<Integer, List<Integer>> map = new HashMap<>();
        for(int i = 0; i < m; i++) {
            int p1 = sc.nextInt();
            int p2 = sc.nextInt();
            List<Integer> l1 = map.getOrDefault(p1, new ArrayList<>());
            l1.add(p2);
            map.put(p1, l1);
            List<Integer> l2 = map.getOrDefault(p2, new ArrayList<>());
            l2.add(p1);
            map.put(p2, l2);
        }
        Set<String> total = new HashSet<>();
        for(int i = 0; i < n; i++) {
            List<Integer> l = new ArrayList<>();
            l.add(i + 1);
            boolean[] visited = new boolean[n + 1];
            visited[i + 1] = true;
            helper(total, 1, i + 1, map, visited, l);
        }
        System.out.println(total.size());
    }

    private static void helper(Set<String> total, int step, int cur, Map<Integer, List<Integer>> map, boolean[] visited, List<Integer> l) {
        if(step == 5) {
            List<Integer> copy = new ArrayList<>(l);
            Collections.sort(copy);
            total.add(copy.toString());
            return;
        }
        for(int next: map.getOrDefault(cur, new ArrayList<>())) {
            if(visited[next]) {
                continue;
            }
            visited[next] = true;
            l.add(next);
            helper(total, step + 1, next, map, visited, l);
            l.remove(l.size() - 1);
            visited[next] = false;
        }
    }



    private static void method3() {
        Scanner sc = new Scanner(System.in);

    }


    /*
    100%
    其中，输入0的话则会输入空格。
    你的任务是处理一段输入序列从而输出用户真正输入的内容。这段序列会显示每一秒用户按下的按键，如2表示按下2键，0表示按下0键，而空格表示这一秒用户没有输入。
    如果该用户2秒内无输入，或者按下与上一按键不同的按键的话那么整个键盘会被“刷新”，即忽略上一次的用户按键。如果该键仅含有3个字母，但是用户按下了4次该键，那么又会回到该键的第一个字母。
    例如，连续按4下6键，那么此时输入的字母为m，连续按5次7键则输入的字母为p。
    接下来，给你一段长度为n的仅含有-和除1之外的阿拉伯数字组成的字符串，你需要输出用户真正输入的文本内容。保证输入的字符串合法。本题中用字符 - 来表示空格，详情请见样例。
    输入含有多组数据，第一行一个正整数T，表示数据组数。
    每一组数据包含两行，第一行为字符串长度n，第二行为一长度为n的字符串。


    5
    4
    2233
    10
    0202020202
    15
    2--2-2---222--2
    12
    400-820-8820
    38
    00002222333344445555666677777888899999

    be
    -a-a-a-a-a
    abca
    g--ta-ua-
    ----adgjmptw
     */
    private static void method4() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] input = new String[n];
        for(int i = 0; i < n; i++) {
            int len = sc.nextInt();
            sc.nextLine();
            input[i] = sc.nextLine();
        }
        System.out.println(Arrays.toString(input));
        for(int i = 0; i < n; i++) {
            System.out.println(process(input[i]));
        }
    }

    private static String process(String s) {
        StringBuilder sb = new StringBuilder();
        char[] cs = s.toCharArray();
        int index = 0;
        while(index < cs.length) {
            if(cs[index] == '-') {
                index++;
                continue;
            }
            if(cs[index] == '0') {
                sb.append('-');
                index++;
                continue;
            }
            String target = map[cs[index] - '0'];
            int i = index + 1;
            if(i == cs.length) {
                sb.append(target.charAt(0));
                index++;
            }
            //duplicate
            else if(cs[i] == cs[index] || i < cs.length - 1 && cs[i] == '-' && cs[i + 1] == cs[index]) {
                int count = 1;
                while(i < cs.length && cs[i] == cs[index] || i < cs.length - 1 && cs[i] == '-' && cs[i + 1] == cs[index]) {
                    count++;
                    i = cs[i] == '-' ? i + 2 : i + 1;
                }
                int pos = count % target.length();
                pos = pos == 0 ? target.length() - 1 : pos - 1;
                char c = target.charAt(pos);
                sb.append(c);
                index = i;
            }
            //stop
            else if(cs[i] == '-') {
                while(i < 0 && cs[i] == '-') {
                    i++;
                }
                char c = target.charAt(0);
                sb.append(c);
                index = i;
            }
            //skip
            else {
                sb.append(target.charAt(0));
                index++;
            }
        }
        return sb.toString();
    }

    private static String[] map = {
        "-", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"
    };

    /*
    18%
    给出一棵树，树上的每个点都有一个以数字表示的颜色，请问有多少点对之间的路径包含所有的四种颜色？
    第一行一个数n(1≤n≤100000)，代表树的点数
    接下来一行 n个空格隔开的数字，代表每个点上的颜色。我们将四种颜色用数字1、2、3、4代表。
    接下来 n-1 行，每行两个空格隔开的数字u,v(1≤u,v≤n,u≠v)，代表树上的每个边连接的两个点。

    5
    1 2 3 4 1
    1 2
    2 3
    3 4
    4 5

    6
     */
    private static void method5() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] color = new int[n + 1];
        for(int i = 1; i <= n; i++) {
            color[i] = sc.nextInt();
        }
        Map<Integer, List<Integer>> map = new HashMap<>();
        for(int i = 0; i < n - 1; i++) {
            int p1 = sc.nextInt();
            int p2 = sc.nextInt();
            List<Integer> l1 = map.getOrDefault(p1, new ArrayList<>());
            l1.add(p2);
            map.put(p1, l1);
            List<Integer> l2 = map.getOrDefault(p2, new ArrayList<>());
            l2.add(p1);
            map.put(p2, l2);
        }
        Set<String> set = new HashSet<>();
        for(int i = 1; i <= n; i++) {
            boolean[] visited = new boolean[n + 1];
            visited[i] = true;
            Map<Integer, Integer> c = new HashMap<>();
            c.put(i, color[i]);
            helper(set, map, i, i, color, c, visited);
        }
//        System.out.println(set);
        System.out.println(set.size());
    }

    private static void helper(Set<String> set, Map<Integer, List<Integer>> map, int start, int cur, int[] color, Map<Integer, Integer> c, boolean[] visited) {
        if(c.size() == 4) {
            String s = start + ":" + cur;
            if(set.add(s)) {
                set.add(cur + ":" + start);
            }
        }
        for(int next: map.getOrDefault(cur, new ArrayList<>())) {
            if(visited[next]) {
                continue;
            }
            c.put(color[next], c.getOrDefault(color[next], 0) + 1);
            visited[next] = true;
            helper(set, map, start, next, color, c, visited);
            visited[next] = false;
            c.put(color[next], c.get(color[next]) - 1);
            if(c.get(color[next]) == 0) {
                c.remove(color[next]);
            }
        }
    }
}
