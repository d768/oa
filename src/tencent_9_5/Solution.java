package tencent_9_5;

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution o = new Solution();
        o.method2();
    }

    /**
    input：链表组
    output：一个链表
    循环链接每个链表的头部元素
    [{1,2,3},{4,5},{7,8,9,10,11,12}]
    输出：{1,4,7,2,5,8,3,9,10,11,12}
    100%
     */
    public ListNode solve (ListNode[] a) {
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;
        int num = a.length;
        if(num == 0) {
            return null;
        }
        int count = 0;
        boolean[] visited = new boolean[num];
        while(count < num) {
            for(int i = 0; i < num; i++) {
                ListNode n = a[i];
                if(n == null) {
                    if(!visited[i]) {
                        visited[i] = true;
                        count++;
                    }
                } else {
                    pre.next = n;
                    a[i] = n.next;
                    pre = pre.next;
                }
            }
        }
        return dummy.next;
    }

    /**
    A和B各有n个精灵，攻击力先A后B，B顺序出战，A最多胜利局数，攻击力因子数较大获胜
    3
    5 8 9
    4 12 6
    输出：1
    100%
     */
    private void method2() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        for(int i = 0; i < n; i++) {
            int num = sc.nextInt();
            a[i] = count(num);
        }
        for(int i = 0; i < n; i++) {
            int num = sc.nextInt();
            b[i] = count(num);
        }
        int res = 0;
        Arrays.sort(a);
        Arrays.sort(b);
        int index = n - 1;
        for(int i = n - 1; i >= 0; i--) {
            if(a[index] > b[i]) {
                res++;
                index--;
            }
        }
        System.out.println(res);
    }

    private int count(int num) {
        Set<Integer> set = new HashSet<>();
        double root = sqr(num);
        for(int i = 1; i <= root; i++) {
            if(num % i == 0) {
                set.add(i);
                set.add(num / i);
            }
        }
        return set.size() == 0 ? 1 : set.size();
    }

    private double sqr(int num) {
        double x = num, y = 0.0;
        while(Math.abs(x - y) > 0.00001) {
            y = x;
            x = 0.5 * (x + num / x);
        }
        return x;
    }

    /**
    给01串，删除任意个数元素后最大价值
    规则：
    i = 0,val[i] = 1
    i > 0: a[i] != a[i-1], val[i] = 1
           a[i] == a[i-1], val[i] = val[i-1] + 1
    6
    010101
    输出：7
    100%
     */
    private void method3() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();
        int[] nums = new int[n];
        int zero = 0;
        for(int i = 0; i < n; i++) {
            nums[i] = s.charAt(i) - '0';
            if(nums[i] == 0) {
                zero++;
            }
        }
        int one = n - zero;
        int res = 1;
        int index = 1;
        int pre = nums[0];
        int preV = 1;
        if(pre == 1) {
            one--;
        } else {
            zero--;
        }
        while(index < n) {
            if (nums[index] == pre) {
                int curV = preV + 1;
                preV = curV;
                res += curV;
            } else {
                if((nums[index] & 1) == 1) {
                    long zAmount = (preV + 1 + preV + zero) * zero / 2;
                    long oAmount = (1 + one) * one / 2;
                    if(zAmount < oAmount) {
                        res += 1;
                        preV = 1;
                        pre = 1;
                    }
                } else {
                    long oAmount = (preV + 1 + preV + one) * one / 2;
                    long zAmount = (1 + zero) * zero / 2;
                    if(zAmount > oAmount) {
                        res += 1;
                        preV = 1;
                        pre = 0;
                    }
                }
            }

            if((nums[index] & 1) == 1) {
                one--;
            } else {
                zero--;
            }
            index++;
        }
        System.out.println(res);
    }

    /**
    input: n l r   n <= 2^50
    每次把n变成三个数，n/2, n%2, n/2，向下取整，直至0/1
    求最终[l-n]区间内，1的个数
    10 3 10
    输出：5
    30%
     */
    private void method4() {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextInt();
        int l = sc.nextInt();
        int r = sc.nextInt();
        List<Long> list = new ArrayList<>();
        List<Long> copy = new ArrayList<>();
        list.add(n);
        boolean flag = true;
        while(flag) {
            flag = false;
            copy.clear();
            for(long num: list) {
                if(num != (long)0 && num != (long)1) {
                    flag = true;
                    long a = num / 2;
                    long b = num % 2;
                    copy.add(a);
                    copy.add(b);
                    copy.add(a);
                } else {
                    copy.add(num);
                }
            }
            List<Long> temp = copy;
            copy = list;
            list = temp;
        }
        long res = 0;
        for(int i = l - 1; i < r; i++) {
            res += list.get(i);
        }
        System.out.println(res);
    }

    /**
    n个数字，求满足条件连续子序列的个数  1 <= n <= 100000
    条件：中间的数字大于等于左右两端的数字
    4
    1 1 2 1
    输出：5
    40%
     */
    private void method5() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums = new int[n];
        for(int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
        }
        int res = 0;
        for(int i = 0; i < n; i++) {
            for(int j = i + 1; j < n; j++) {
                if(j == i + 1) {
                    res++;
                    continue;
                }
                if(valid(nums, i, j)) {
                    res++;
                }
            }
        }
        System.out.println(res);
    }

    private boolean valid(int[] nums, int i, int j) {
        for(int k = i + 1; k < j; k++) {
            if(nums[k] < nums[i] || nums[k] < nums[j]) {
                return false;
            }
        }
        return true;
    }
}

class ListNode {
     int val;
     ListNode next = null;
     public ListNode(int val) {
     this.val = val;
     }
}
